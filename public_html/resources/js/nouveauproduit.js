/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function Product(name, price, description, color, height) {
    this.name = name;
    this.price = price;
    this.description = description;
    this.color = color;
    this.height = height;
}

var keyboard = new Product("keyboard", "50", "for write and navigate on desk", "black", "60cm");
var mouse = new Product("mouse", "15", "for click and navigate on desk", "black", "12cm");
var screen = new Product("screen", "200", "for see and navigate on desk", "black", "80cm");

var catalogue = new Array;
catalogue[0] = keyboard;
catalogue[1] = mouse;
catalogue[2] = screen;
var indice = 0;

function fiche(item) {
    if (item instanceof Product) {
        document.getElementById('name').innerHTML = item.name + "</br>" + item.price + "</br>" + item.description + "</br>" + item.color + "</br>" + item.height;
    } else {
        alert("Vous ne souhaitez pas afficher de produit!");
    }
}

function precedent() {
    indice--;
    if (indice < 0) {
        indice = catalogue.length - 1;
    }
    fiche(catalogue[indice]);
}

function suivant() {
    indice++;
    if (indice >= catalogue.length) {
        indice = 0;
    }
    fiche(catalogue[indice]);
}
fiche(catalogue[indice]);