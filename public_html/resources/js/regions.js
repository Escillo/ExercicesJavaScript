"use strict";
        var generateDepartmentList = new Array("Ain", "Aisne", "Allier", "Alpes-de-Haute-Provence", "Alpes-Maritimes", "Ardèche", "Ardennes", "Ariège", "Aube", "Aude", "Aveyron", "Bas-Rhin", "Bouches-du-Rhône", "Calvados", "Cantal", "Charente", "Charente-Maritime", "Cher", "Corrèze", "Corse-du-Sud", "Côte-d'Or", "Côtes-d'Armor", "Creuse", "Deux-Sèvres", "Dordogne", "Doubs", "Drôme", "Essonne", "Eure", "Eure-et-Loir", "Finistère", "Gard", "Gers", "Gironde", "Guadeloupe", "Guyane", "Haut-Rhin", "Haute-Corse", "Haute-Garonne", "Haute-Loire", "Haute-Marne", "Haute-Saône", "Haute-Savoie", "Haute-Vienne", "Hautes-Alpes", "Hautes-Pyrénées", "Hauts-de-Seine", "Hérault", "Ille-et-Vilaine", "Indre", "Indre-et-Loire", "Isère", "Jura", "La Réunion", "Landes", "Loir-et-Cher", "Loire", "Loire-Atlantique", "Loiret", "Lot", "Lot-et-Garonne", "Lozère", "Maine-et-Loire", "Manche", "Marne", "Martinique", "Mayenne", "Mayotte", "Meurthe-et-Moselle", "Meuse", "Morbihan", "Moselle", "Nièvre", "Nord", "Oise", "Orne", "Paris", "Pas-de-Calais", "Puy-de-Dôme", "Pyrénées-Atlantiques", "Pyrénées-Orientales", "Rhône", "Saône-et-Loire", "Sarthe", "Savoie", "Seine-et-Marne", "Seine-Maritime", "Seine-Saint-Denis", "Somme", "Tarn", "Tarn-et-Garonne", "Territoire de Belfort", "Val-d'Oise", "Val-de-Marne", "Var", "Vaucluse", "Vendée", "Vienne", "Vosges", "Yonne", "Yvelines");
        var d = document.departmentListForm.departmentList;
        for (var i = 0; i < generateDepartmentList.length; i++) {
d.length++;
        d.options[d.length - 1].text = generateDepartmentList[i];
        }

function getRegion(){
//var generateRegionList = new Array("Auvergne-Rhône-Alpes", "Bourgogne-Franche-Comté", "Bretagne", "Centre-Val de Loire", "Corse", "Grand Est", "Hauts-de-France", "Île-de-France", "Normandie", "Nouvelle-Aquitaine", "Occitanie", "Pays de la Loire", "Provence-Alpes-Côtes d'Azur");
var region = document.getElementById("choix").value;
        if (region === "Ain" || region === "Allier" || region === "Ardèche" || region === "Cantal" || region === "Drôme" || region === "Isère" || region === "Loire" || region === "Haute-Loire" || region === "Puy-de-Dôme" || region === "Rhône" || region === "Savoie" || region === "Haute-Savoie"){
document.getElementById('resultat').innerHTML = 'votre département est situé dans la Région Auvergne - Rhône - Alpes';
        }
else if (region === "Côte-d'Or" || region === "Doubs" || region === "Jura" || region === "Nièvre" || region === "Haute-Saône" || region === "Saône-et-Loire" || region === "Yonne"){
document.getElementById('resultat').innerHTML = 'votre département est situé dans la Région Bourgogne-Franche-Comté';
}
else if (region === "Côte-d'Armor" || region === "Finistère" || region === "Ille-et-Vilaine" || region === "Morbihan"){
document.getElementById('resultat').innerHTML = 'votre département est situé dans la Région Bretagne';
        }
else if (region === "Cher" || region === "Eure-et-Loir" || region === "Indre" || region === "Indre-er-Loire" || region === "Loir-et-Cher" || region === "Loiret"){
document.getElementById('resultat').innerHTML = 'votre département est situé dans la Région Centre-Val de Loire';
        }
else if (region === "Haute-Corse" || region === "Corse-du-Sud"){
document.getElementById('resultat').innerHTML = 'votre département est situé dans la Région Corse';
        }
else if (region === "Ardennes" || region === "Aube" || region === "Marne" || region === "Haute-Marne" || region === "Meurthe-et-Moselle" || region === "Meuse" || region === "Moselle" || region === "Bas-Rhin" || region === "Haut-Rhin" || region === "Vosges"){
document.getElementById('resultat').innerHTML = 'votre département est situé dans la Région Grande-Est';
}
else if (region === "Aisne" || region === "Nord" || region === "Oise" || region === "Pas-de-Calais" || region === "Somme"){
document.getElementById('resultat').innerHTML = 'votre département est situé dans la Région Hauts-de-France';
}
else if (region === "Paris" || region === "Seine-et-Marne" || region === "Yvelines" || region === "Essonne" || region === "Haut-de-Seine" || region === "Seine-Saint-Denis" || region === "Val-de-Marne" || region === "Val-d'Oise"){
document.getElementById('resultat').innerHTML = 'votre département est situé dans la Région Île-de-France';
        }
else if (region === "Calvados" || region === "Eure" || region === "Manche" || region === "Orne" || region === "Seine-Maritime"){
document.getElementById('resultat').innerHTML = 'votre département est situé dans la Région Normandie';
        }
else if (region === "Charente" || region === "Charente-Maritime" || region === "Corrèze" || region === "Creuse" || region === "Dordogne" || region === "Gironde" || region === "Landes" || region === "Lot-et-Garonne" || region === "Pyrénées-Atlantiques" || region === "Deux-Sèvres" || region === "Vienne" || region === "Haute-Vienne"){
document.getElementById('resultat').innerHTML = 'votre département est situé dans la Région Nouvelle-Aquitaine';
        }
else if (region === "Ariège" || region === "Aude" || region === "Aveyron" || region === "Gard" || region === "Haute-Garonne" || region === "Gers" || region === "Hérault" || region === "Lot" || region === "Lozère" || region === "Hautes-Pyrénées" || region === "Pyrénées-Orientales" || region === "Tarn" || region === "Tarn-et-Garonne"){
document.getElementById('resultat').innerHTML = 'votre département est situé dans la Région Occitanie';
        }
else if (region === "Loire-Atlantique" || region === "Maine-et-Loire" || region === "Mayenne" || region === "Sarthe" || region === "Vendée"){
document.getElementById('resultat').innerHTML = 'votre département est situé dans la Région Pays de la Loire';
}
else if (region === "Alpes-de-Haute-Provence" || region === "Hautes-Alpes" || region === "Alpes-Maritimes" || region === "Bouches-du-Rhône" || region === "Var" || region === "Vaucluse"){
document.getElementById('resultat').innerHTML = 'votre département est situé dans la Région Provence-Alpes-Côte d\'Azur';
}
else{
document.getElementById('resultat').innerHTML = 'votre département est situé dans les DOM-TOM';
        }
}
