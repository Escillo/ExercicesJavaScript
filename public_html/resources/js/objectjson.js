/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function Product(name, price, description, size) {
    this.name = name;
    this.price = price;
    this.description = description;
    this.size = size;
}

var plate = new Product("assiette", "30€", "sert à mettre la nourriture", "20");
var fork = new Product("fourchette", "1€", "sert à piquer la nourriture", "12");
var knife = new Product("couteau", "2€", "sert à couper la nourriture", "15");

var kitchen = [plate, fork, knife];

console.log(plate);
console.log(fork);
console.log(knife);
var JSONplate = JSON.stringify(plate);

console.log(JSONplate);
console.log(JSONplate.name);


var glass = "{\"name\": \"verre\", \"price\": \"30€\",\"description\": \"sert à mettre la nourriture\", \"size\":20}";
var JSONglass = JSON.parse(glass);
console.log(JSONglass.name);

